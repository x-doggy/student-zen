# Student.Zen

Website recommends useful resources for students needed during studying in university.

![Index page](doc/screenshot-zen-index.png)

![Register page](doc/screenshot-zen-register.png)

![Login page](doc/screenshot-zen-login.png)

![Profile page](doc/screenshot-zen-profile.png)

![Recommendation page](doc/screenshot-zen-recommendation.png)

Also, check out [My presentation](https://x-doggy.gitlab.io/student-zen-pres-shower/) on Russian representing my qualifying work.

## Build project

This project is built by [Maven](http://maven.apache.org/) and uses [MariaDB](https://mariadb.com/) as common database.

First, be sure that MariaDB daemon working, ``student_zen`` database exists and ``mvn`` command available from terminal.

Then, do following command from terminal to build ``jar``-file:

```
mvn clean package
```

## Launch project

Run a ``jar``-file built from previous step:

```
java -jar target/student-zen*.jar
```

You can also use this command to launch project from terminal without pre-packaging.

```
mvn clean spring-boot:run
```

## Build project in NetBeans IDE

You should not to use default ``Run`` command In NetBeans. Instead, change command from Properties menu as shown in [Oracle blog post](https://blogs.oracle.com/geertjan/spring-boot-scenario-with-netbeans-ide).

## Technologies

| Property | Technology |
| -------- | ---------- |
| Common language | [Java](https://java.com/) |
| Dependency manager | [Maven](http://maven.apache.org/) |
| Development platform | [Spring](https://spring.io/) |
| Web framework | [Spring Boot](https://spring.io/projects/spring-boot) |
| Database | [MariaDB](https://mariadb.com/) |
| Template engine | [Freemarker](https://freemarker.apache.org/) |


## Author

© 2018, Vladimir Stadnik. [My blog](https://x-doggy.gitlab.io/) and [Telegram](https://t.me/x_doggy).
