package com.stadnik.zen.controllers;

import com.stadnik.zen.entities.Specialities;
import com.stadnik.zen.entities.TeachingGroup;
import com.stadnik.zen.entities.Usr;
import com.stadnik.zen.repositories.SpecialitiesRepository;
import com.stadnik.zen.repositories.TeachingGroupRepository;
import com.stadnik.zen.services.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AuthenticationController {

  private final UserService userService;
  private final TeachingGroupRepository teachingGroupRepository;
  private final SpecialitiesRepository specialitiesRepository;

  @Autowired
  public AuthenticationController(UserService userService,
                                  TeachingGroupRepository teachingGroupRepository,
                                  SpecialitiesRepository specialitiesRepository) {
    this.userService = userService;
    this.teachingGroupRepository = teachingGroupRepository;
    this.specialitiesRepository = specialitiesRepository;
  }

  @GetMapping("/register")
  public String renderRegisterPage(@ModelAttribute("model") ModelMap model) {
    List<Specialities> specialitiesList = specialitiesRepository.findAll();
    List<TeachingGroup> teachingGroupsList = teachingGroupRepository.findAll();
    model.addAttribute("specialitiesList", specialitiesList);
    model.addAttribute("teachingGroupsList", teachingGroupsList);
    return "register";
  }

  @PostMapping("/register")
  public String addUser(Usr user, @ModelAttribute("model") ModelMap model) {
    if (!userService.addUser(user)) {
      List<Specialities> specialitiesList = specialitiesRepository.findAll();
      List<TeachingGroup> teachingGroupsList = teachingGroupRepository.findAll();
      model.addAttribute("specialitiesList", specialitiesList);
      model.addAttribute("teachingGroupsList", teachingGroupsList);
      model.addAttribute("message", "User exists!");
      return "register";
    }
    return "redirect:/login";
  }
  
  @GetMapping("/login")
  public String renderLoginPage() {
    return "login";
  }
  
}
