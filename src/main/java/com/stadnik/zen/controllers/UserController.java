package com.stadnik.zen.controllers;

import com.stadnik.zen.entities.Role;
import com.stadnik.zen.entities.Specialities;
import com.stadnik.zen.entities.TeachingGroup;
import com.stadnik.zen.repositories.TeachingGroupRepository;
import com.stadnik.zen.entities.Usr;
import com.stadnik.zen.repositories.SpecialitiesRepository;
import com.stadnik.zen.services.UserService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/user")
public class UserController {

  private final TeachingGroupRepository teachingGroupRepository;
  private final SpecialitiesRepository specialitiesRepository;
  private final UserService userService;

  @Autowired
  public UserController(TeachingGroupRepository teachingGroupRepository,
                        SpecialitiesRepository specialitiesRepository,
                        UserService userService) {
    this.teachingGroupRepository = teachingGroupRepository;
    this.specialitiesRepository = specialitiesRepository;
    this.userService = userService;
  }

  @GetMapping()
  public String userAdminPage(@AuthenticationPrincipal Usr usr, Model model) {
    List<TeachingGroup> teachingGroupsList = teachingGroupRepository.findAll();
    List<Specialities> specialitiesList = specialitiesRepository.findAll();
    model.addAttribute("teachingGroupsList", teachingGroupsList);
    model.addAttribute("specialitiesList", specialitiesList);
    model.addAttribute("usr", usr);
    model.addAttribute("roles", Role.values());
    return "user";
  }
  
  @PostMapping()
  public String userAdminSave(
          @RequestParam String username,
          @RequestParam Map<String, String> form,
          @RequestParam("userId") Usr usr
          ) {

    userService.saveUser(usr, username, form);
    return "redirect:/";
  }
  
}
