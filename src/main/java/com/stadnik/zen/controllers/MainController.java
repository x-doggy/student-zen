package com.stadnik.zen.controllers;

import com.stadnik.zen.entities.Usr;
import com.stadnik.zen.repositories.RecommendationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

  @Autowired
  private RecommendationRepository recommendationRepository;

  @GetMapping("/")
  public String index(@AuthenticationPrincipal Usr usr, Model model) {
    model.addAttribute("recommendations", recommendationRepository.findAll());
    model.addAttribute("mainRecommendation", recommendationRepository.findByUrl("https://www.intuit.ru/studies/courses/37/37/info"));
    return "index";
  }

  @GetMapping("/403")
  public String error403() {
    return "errors/403";
  }
  
}
