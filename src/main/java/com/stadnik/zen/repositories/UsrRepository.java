package com.stadnik.zen.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.stadnik.zen.entities.Usr;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "usrs", path = "usrs")
public interface UsrRepository extends JpaRepository<Usr, Long> {
  Usr findByUsername(String username);
}
