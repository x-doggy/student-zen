package com.stadnik.zen.repositories;

import com.stadnik.zen.entities.University;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "universities", path = "universities")
public interface UniversityRepository extends JpaRepository<University, Long> {
  University findByTitle(String title);
}
