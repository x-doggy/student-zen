package com.stadnik.zen.repositories;

import com.stadnik.zen.entities.Tags;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "tags", path = "tags")
public interface TagsRepository extends JpaRepository<Tags, Long> {
  Tags findByTag(String tag);
}
