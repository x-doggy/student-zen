package com.stadnik.zen.repositories;

import com.stadnik.zen.entities.Specialities;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "specialities", path = "specialities")
public interface SpecialitiesRepository extends JpaRepository<Specialities, Long> {
  Specialities findByTitle(String title);
}
