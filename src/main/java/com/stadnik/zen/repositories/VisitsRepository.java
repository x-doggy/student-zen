package com.stadnik.zen.repositories;

import com.stadnik.zen.entities.Visits;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "visits", path = "visits")
public interface VisitsRepository extends JpaRepository<Visits, Long> {
  Visits findByVisitDate(Date date);
}
