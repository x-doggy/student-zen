package com.stadnik.zen.repositories;

import com.stadnik.zen.entities.TeachingGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "teachinggroups", path = "teachinggroups")
public interface TeachingGroupRepository extends JpaRepository<TeachingGroup, Long> {
  TeachingGroup findByTitle(String title);
}
