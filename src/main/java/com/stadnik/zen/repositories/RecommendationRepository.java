package com.stadnik.zen.repositories;

import com.stadnik.zen.entities.Recommendation;
import com.stadnik.zen.entities.Specialities;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.ModelAttribute;

@RepositoryRestResource(collectionResourceRel = "recommendations", path = "recommendations")
public interface RecommendationRepository extends JpaRepository<Recommendation, Long> {
  Recommendation findByUrl(String url);

  //@Query("from Recommendation where specialities like :sp")
  //Recommendation findBySpecialities(@ModelAttribute("sp") Specialities specialities);
}
