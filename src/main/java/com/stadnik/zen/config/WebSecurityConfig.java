package com.stadnik.zen.config;

import com.stadnik.zen.services.UserService;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private UserService userService;

  @Bean
  protected PasswordEncoder getBCryptPasswordEncoder() {
    return new BCryptPasswordEncoder(8);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
            .authorizeRequests()
            .antMatchers(
                    /* Regular URLs */
                    "/", "/register", "/login", "/h2-console/**",
                    /* Project resources folders */
                    "/resources/**", "/static/**",
                    /* JPA REST resources */
                    "/usrs/**", "/recommendations/**",
                    "/universities/**", "/specialities/**",
                    "/teachinggroups/**", "/visits/**", "/tags/**"
            ).permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin().loginPage("/login").permitAll()
            .and()
            .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")
            .and()
            .headers().frameOptions().disable();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userService)
            .passwordEncoder(getBCryptPasswordEncoder());
  }
  
  @Override
  public void configure(WebSecurity web) {
    web.ignoring().antMatchers("/css/**");
    web.ignoring().antMatchers("/img/**");
    web.ignoring().antMatchers("/js/**");
    web.ignoring().mvcMatchers(HttpMethod.POST,
            "/usrs/**", "/recommendations/**",
            "/universities/**", "/specialities/**",
            "/teachinggroups/**", "/visits/**", "/tags/**");
    web.ignoring().mvcMatchers(HttpMethod.PUT,
            "/usrs/**", "/recommendations/**",
            "/universities/**", "/specialities/**",
            "/teachinggroups/**", "/visits/**", "/tags/**");
    web.ignoring().mvcMatchers(HttpMethod.PATCH,
            "/usrs/**", "/recommendations/**",
            "/universities/**", "/specialities/**",
            "/teachinggroups/**", "/visits/**", "/tags/**");
  }
}
