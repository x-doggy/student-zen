package com.stadnik.zen.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;

@Entity
@Data
public class Visits implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Temporal(TemporalType.TIMESTAMP)
  private Date visitDate;

  @JoinColumn(name = "FK_USR")
  @OneToOne(cascade = CascadeType.ALL)
  private Usr fkUsr;

  @JoinColumn(name = "FK_RECOMMENDATION")
  @OneToOne(cascade = CascadeType.ALL)
  private Recommendation fkRecommendation;
}
