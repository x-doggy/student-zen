package com.stadnik.zen.entities;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import lombok.Data;

@Entity
@Data
public class Recommendation implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  private String url;
  private String title;
  
  @Column(columnDefinition = "TEXT")
  private String description;
  
  @OneToOne(cascade = CascadeType.ALL)
  private Specialities specialities;
  
}
