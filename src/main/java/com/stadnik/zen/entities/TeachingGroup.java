package com.stadnik.zen.entities;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import lombok.Data;

@Entity
@Data
public class TeachingGroup implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String title;

  @OneToOne(mappedBy = "fkTeachingGroup")
  private Usr fkUsr;
  
  @JoinColumn(name = "FK_UNIVERSITY")
  @OneToOne(cascade = CascadeType.ALL)
  private University fkUniversity;
}
