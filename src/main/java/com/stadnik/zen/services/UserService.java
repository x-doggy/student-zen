package com.stadnik.zen.services;

import com.stadnik.zen.entities.Role;
import com.stadnik.zen.entities.Specialities;
import com.stadnik.zen.entities.TeachingGroup;
import com.stadnik.zen.entities.Usr;
import com.stadnik.zen.repositories.SpecialitiesRepository;
import com.stadnik.zen.repositories.TeachingGroupRepository;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import com.stadnik.zen.repositories.UsrRepository;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

  private final UsrRepository userRepository;
  private final TeachingGroupRepository teachingGroupRepository;
  private final SpecialitiesRepository specialitiesRepository;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public UserService(UsrRepository userRepository,
                     TeachingGroupRepository teachingGroupRepository,
                     SpecialitiesRepository specialitiesRepository,
                     PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.teachingGroupRepository = teachingGroupRepository;
    this.specialitiesRepository = specialitiesRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Usr userFromDb = userRepository.findByUsername(username);

    if (userFromDb == null) {
      throw new UsernameNotFoundException("User not found");
    }

    return userFromDb;
  }

  public List<Usr> findAll() {
    return userRepository.findAll();
  }

  public void saveUser(Usr usr, String username, Map<String, String> form) {
    usr.setUsername(username);
    usr.setFullName(form.get("fullName"));
    usr.setEmail(form.get("email"));
    usr.setEducation(form.get("education"));

    String birthFromForm = form.get("birth");
    if (birthFromForm != null && !birthFromForm.equals("")) {
      usr.setBirth(LocalDate.parse(birthFromForm));
    }

    String specialityFromForm = form.get("fkSpeciality");
    if (specialityFromForm != null && !specialityFromForm.equals("")) {
      Optional<Specialities> gotSpecialities =
          specialitiesRepository.findById(Long.parseLong(specialityFromForm));
      gotSpecialities.ifPresent(usr::setFkSpeciality);
    }

    String teachingGroupFromForm = form.get("fkTeachingGroup");
    if (teachingGroupFromForm != null && !teachingGroupFromForm.equals("")) {
      Optional<TeachingGroup> gotTeachingGroup =
          teachingGroupRepository.findById(Long.parseLong(teachingGroupFromForm));
      gotTeachingGroup.ifPresent(usr::setFkTeachingGroup);
    }

    Set<String> roles = Arrays.stream(Role.values())
            .map(Role::name)
            .collect(Collectors.toSet());

    usr.getRoles().clear();

    form.keySet().stream()
        .filter((key) -> (roles.contains(key)))
        .forEachOrdered((key) -> usr.getRoles().add(Role.valueOf(key))
    );

    userRepository.save(usr);
  }

  public boolean addUser(Usr user) {
    Usr userFromDb = userRepository.findByUsername(user.getUsername());

    if (userFromDb != null) {
      return false;
    }

    String hashedPassword = passwordEncoder.encode(user.getPassword());
    user.setPassword(hashedPassword);
    user.setRoles(Collections.singleton(Role.USER));
    userRepository.save(user);

    return true;
  }
}
