-- MariaDB dump 10.19  Distrib 10.7.3-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: zen
-- ------------------------------------------------------
-- Server version	10.7.3-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Session`
--

DROP TABLE IF EXISTS `Session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Session` (
  `sid` varchar(32) NOT NULL,
  `expires` datetime DEFAULT NULL,
  `data` text DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Session`
--

LOCK TABLES `Session` WRITE;
/*!40000 ALTER TABLE `Session` DISABLE KEYS */;
INSERT INTO `Session` VALUES
('5n22VzwFuUsdTG-NGHds3_a94zvhWl92','2018-11-29 15:52:36','{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}');
/*!40000 ALTER TABLE `Session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recommendation`
--

DROP TABLE IF EXISTS `recommendation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommendation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `specialities_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgak8xtpo1wx5jmvlvk0pm1h53` (`specialities_id`),
  CONSTRAINT `FKgak8xtpo1wx5jmvlvk0pm1h53` FOREIGN KEY (`specialities_id`) REFERENCES `specialities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recommendation`
--

LOCK TABLES `recommendation` WRITE;
/*!40000 ALTER TABLE `recommendation` DISABLE KEYS */;
INSERT INTO `recommendation` VALUES
(1,'Академия ALT Linux: Операционная система Linux','В курсе даются основные понятия операционной системы Linux и важнейшие навыки работы в ней. Изложение сопровождается большим количеством практических примеров. Данный курс может рассматриваться как учебник для студентов, начинающих обучение по специальностям в области информатики и ещё не знакомых с ОС Linux.\r\nВ первой части курса вводятся основные понятия и навыки, необходимые пользователю для того, чтобы начать грамотно работать в Linux. Здесь рассмотрены: пользователи с точки зрения системы, понятие терминал и работа с командной строкой, устройство файловой системы и работа с ней, права доступа в Linux, возможности командной оболочки, текстовые редакторы. Вторая часть посвящена тем понятиям и навыкам, которые требуются для администрирования ОС Linux. Сюда входит обсуждение этапов загрузки системы, технологий работы с внешними устройствами, файловыми системами и сетью в Linux, администрирование системы посредством конфигурационных файлов, управление пакетами. В завершающей лекции курса даётся обзор истории возникновения и развития Linux. Здесь же приведён обзор социального контекста, существенного для понимания ОС Linux и работы в ней: сообщество пользователей, лицензирование свободного программного обеспечения, место свободного ПО на современном рынке, дистрибутивы Linux и решения на базе Linux.','https://www.intuit.ru/studies/courses/37/37/info',1),
(2,'Основы операционных систем','В курсе описаны фундаментальные принципы проектирования и реализации операционных систем.\r\nКурс базируется на семестровом курсе «Введение в операционные системы», читаемом авторами в МФТИ и может рассматриваться как учебник для студентов, специализирующихся в области информатики.','https://www.intuit.ru/studies/courses/2192/31/info',1),
(3,'Программирование в Linux с нуля','Эта книга находится в стадии написания и постоянного обновления. Целью создания такой \"дополняемой\" книги стало осознание многогранности феномена программирования в Linux.','https://www.opennet.ru/docs/RUS/zlp/',1),
(4,'Введение в GNU Octave','Курс посвящен свободно распространяемому пакету Octave.','https://www.intuit.ru/studies/courses/3677/919/info',1);
/*!40000 ALTER TABLE `recommendation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialities`
--

DROP TABLE IF EXISTS `specialities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialities`
--

LOCK TABLES `specialities` WRITE;
/*!40000 ALTER TABLE `specialities` DISABLE KEYS */;
INSERT INTO `specialities` VALUES
(1,'Прикладная математика и информатика'),
(2,'Радиофизика'),
(3,'Химия'),
(4,'Безопасность компьютерных систем');
/*!40000 ALTER TABLE `specialities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES
(1,'linux'),
(2,'gnu'),
(3,'octave');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teaching_group`
--

DROP TABLE IF EXISTS `teaching_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teaching_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `fk_university` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKenboyvhg49vmbbxi3ca54wei3` (`fk_university`),
  CONSTRAINT `FKenboyvhg49vmbbxi3ca54wei3` FOREIGN KEY (`fk_university`) REFERENCES `university` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teaching_group`
--

LOCK TABLES `teaching_group` WRITE;
/*!40000 ALTER TABLE `teaching_group` DISABLE KEYS */;
INSERT INTO `teaching_group` VALUES
(1,'МПБ-403-О',1),
(2,'МПБ-402-О',1),
(3,'КТО-142',2);
/*!40000 ALTER TABLE `teaching_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `university`
--

DROP TABLE IF EXISTS `university`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `university` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `university`
--

LOCK TABLES `university` WRITE;
/*!40000 ALTER TABLE `university` DISABLE KEYS */;
INSERT INTO `university` VALUES
(1,'ОмГУ'),
(2,'ОмГТУ'),
(3,'ОмГУПС'),
(4,'ОмГПУ');
/*!40000 ALTER TABLE `university` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `roles` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  KEY `FKfpm8swft53ulq2hl11yplpr5` (`user_id`),
  CONSTRAINT `FKfpm8swft53ulq2hl11yplpr5` FOREIGN KEY (`user_id`) REFERENCES `usr` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES
(1,'ADMIN'),
(1,'USER');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usr`
--

DROP TABLE IF EXISTS `usr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `birth` date DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `fk_speciality` bigint(20) DEFAULT NULL,
  `fk_teaching_group` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3d8mrvk1hrlcfhixbbrcvkcgj` (`fk_speciality`),
  KEY `FKdiegq2pgqw7luvf0io45jvfnv` (`fk_teaching_group`),
  CONSTRAINT `FK3d8mrvk1hrlcfhixbbrcvkcgj` FOREIGN KEY (`fk_speciality`) REFERENCES `specialities` (`id`),
  CONSTRAINT `FKdiegq2pgqw7luvf0io45jvfnv` FOREIGN KEY (`fk_teaching_group`) REFERENCES `teaching_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usr`
--

LOCK TABLES `usr` WRITE;
/*!40000 ALTER TABLE `usr` DISABLE KEYS */;
INSERT INTO `usr` VALUES
(1,'1995-02-22','Высшее','x-doggy@ya.ru','Владимир Стадник','$2a$08$50RCbtbkmX86mVgsRdL2GOvxjmEGxTu3ZJXqI9l8Riq4ZTJAQiBNi','x-doggy',1,1),
(2,'1996-01-01','Начальное','turovets.b@gmail.com','Turovets Bogdan','$2a$08$gtabb3E3Zyu7fgofDXdfwOa3.idCjxvo9Kqoa.iIENxbwDriYj/DW','djb',2,2);
/*!40000 ALTER TABLE `usr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visits`
--

DROP TABLE IF EXISTS `visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_date` datetime DEFAULT NULL,
  `fk_recommendation` bigint(20) DEFAULT NULL,
  `fk_usr` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKt18m42jwd76q3c9nuyokl8m26` (`fk_recommendation`),
  KEY `FKbpr5algf5q7cqghagfnr5grce` (`fk_usr`),
  CONSTRAINT `FKbpr5algf5q7cqghagfnr5grce` FOREIGN KEY (`fk_usr`) REFERENCES `usr` (`id`),
  CONSTRAINT `FKt18m42jwd76q3c9nuyokl8m26` FOREIGN KEY (`fk_recommendation`) REFERENCES `recommendation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits`
--

LOCK TABLES `visits` WRITE;
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'zen'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-01 20:50:32
