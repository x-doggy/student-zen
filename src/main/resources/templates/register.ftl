<#import "_partials/page.ftl" as page>
<#import "_partials/login.ftl" as l>

<@page.page>
  ${message!}
  <@l.login "/register" true />
</@page.page>