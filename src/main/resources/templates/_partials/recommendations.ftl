<#macro recommendation title="" url="" domain="" smalldescription="">

<article class="recommendation-item">
  <h4 class="recommendation-item__title">${title}</h4>
  <div class="recommendation-item__description">${smalldescription}</div>
  <a href="${url}" class="recommendation-item__link" target="_blank">${domain}</a>
</article>

</#macro>



<#macro commonRecommendation title="" url="">

<article class="common-recommendation">
  <h3 class="common-recommendation__title">
    <a href="${url}" class="common-recommendation__link" target="_blank">${title}</a>
  </h3>
  <div class="common-recommendation__description"><#nested /></div>
</article>

</#macro>