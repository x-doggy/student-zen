<#macro page>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Main | Student.Zen</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="stylesheet" href="/css/style-zen.css">
</head>
<#include "security.ftl" />
<body class="page">
  <div class="page-layout">
    <header class="page-layout__header">
      <h1 class="page-layout__header-title">
        <a href="/" class="page-layout__header-title__link">Student.Zen</a>
      </h1>
      <div class="page-layout__rightlinks">
        <nav class="page-layout__nav">
          <ul class="page-layout__nav-links">
            <#if username != "unknown">
            <li class="page-layout__nav-item">
              <a class="page-layout__nav-link" href="/user/">${user.getFullName()}</a>
            </li>
            <#else>
            <li class="page-layout__nav-item">
              <a href="/login" class="page-layout__nav-link">Войти</a>
            </li>
            <li class="page-layout__nav-item">
              <a href="/register" class="page-layout__nav-link">Зарегистрироваться</a>
            </li>
            </#if>
          </ul>
        </nav>
        <a href="https://gitlab.com/x-doggy/student-zen" class="page-layout__rightlinks-src" target="_blank">Source code</a>
      </div>
    </header>
    <main class="page-main">
      <div class="page-main__content">
        <#nested />
      </div>
    </main>
  </div>
</body>
</html>
</#macro>