<#include "security.ftl" />

<#macro login path isRegisterForm>
<#if username != "unknown">
  <div>Вы уже вошли в систему, ${username}! Вернуться <a href="/">на домашнюю страницу</a>?</div>
<#else>
<form action="${path}" method="post" class="login-form">
  <h4 class="login-form__title">
    <#if isRegisterForm>Регистрация<#else>Вход в систему</#if>
  </h4>
  <div class="login-form__item">
    <input type="text" name="username" id="username" placeholder="Имя пользователя" required class="login-form__inp">
  </div>
  <div class="login-form__item">
    <input type="password" name="password" id="password" placeholder="Пароль" required class="login-form__inp">
  </div>
  <#if isRegisterForm>
  <div class="login-form__item">
    <input type="email" name="email" placeholder="ваш@email.com" required class="login-form__inp">
  </div>
  <div class="login-form__item">
    <input type="text" name="fullName" placeholder="Полное имя" class="login-form__inp">
  </div>
  <div class="login-form__item">
    <input type="date" name="birth" placeholder="Дата рождения" class="login-form__inp">
  </div>
  <div class="login-form__item">
    <input type="text" name="education" placeholder="Имеющееся образование" class="login-form__inp">
  </div>
  <#if model["specialitiesList"]??>
  <div class="login-form__item">
    <select name="fkSpeciality">
      <option value="" selected disabled>Выберите специальность</option>
      <#list model["specialitiesList"] as sl>
      <option value="${sl.id}">${sl.title}</option>
      </#list>
    </select>
  </div>
  </#if>
  <#if model["teachingGroupsList"]??>
  <div class="login-form__item">
    <select name="fkTeachingGroup" class="simple-form__item--input">
      <option value="" selected disabled>Выберите учебную группу</option>
      <#list model["teachingGroupsList"] as tgl>
      <option value="${tgl.id}">${tgl.title}</option>
      </#list>
    </select>
  </div>
  </#if>
  
  </#if>
  <div class="login-form__block">
    <button type="submit" class="login-form__inp login-form__btn">
    <#if isRegisterForm>Зарегистрироваться!<#else>Войти!</#if>
    </button>
  </div>
  <div class="login-form__block">
  <#if isRegisterForm><a href="/login">Войти!</a><#else><a href="/register">Регистрация</a></#if>
  </div>
  <input type="hidden" name="_csrf" value="${_csrf.token}">
</form>
</#if>
</#macro>

<#macro logout>
<form action="/logout" method="post">
  <input type="hidden" name="_csrf" value="${_csrf.token}" />
  <button type="submit">Выйти!</button>
</form>
</#macro>