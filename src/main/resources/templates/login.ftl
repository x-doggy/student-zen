<#import "_partials/page.ftl" as page>
<#import "_partials/login.ftl" as l>

<@page.page>
  ${message?ifExists}
  <@l.login "/login" false />
</@page.page>