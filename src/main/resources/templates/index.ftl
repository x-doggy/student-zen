<#import "_partials/page.ftl" as page />
<#import "_partials/recommendations.ftl" as r />
<#include "_partials/security.ftl" />

<@page.page>
  <h3 class="page-main__title">
    <#if username != "unknown">
    ${user.getFullName()}, добро пожаловать в Student.Zen!
    <#else>
    Добро пожаловать в Student.Zen!
    </#if>
  </h3>

  <section class="this-recommendation">
    <@r.commonRecommendation
      url="${mainRecommendation.url}"
      title="${mainRecommendation.title}">
      <#list (mainRecommendation.description)?split("\n") as pItem>
        <p>${pItem}</p>
      </#list>
    </@r.commonRecommendation>
  </section>

  <h3>Рекомендуем:</h3>

  <section class="recommendations">
  <#-- Print recommendations -->
  <#list recommendations as item>
    <#if item.url == mainRecommendation.url>
      <#continue>
    <#else>
      <@r.recommendation
      title="${item.title}"
      url="${item.url}"
      domain="${item.url}"
      smalldescription="${item.description}" />
    </#if>
  </#list>
  </section>

</@page.page>