<#import "_partials/page.ftl" as page />

<@page.page>
  
  <form action="/user" method="POST" class="simple-form">
    
    <h3 class="page-main__title">Профиль пользователя</h3>

    <input type="hidden" value="${usr.id}" name="userId">
    <input type="hidden" value="${_csrf.token}" name="_csrf">
    
    <ul class="simple-form__wrapper">
      <li class="simple-form__item">
        <label for="username" class="simple-form__item--label">Логин:</label>
        <input type="text" value="${usr.username}" name="username" id="username" class="simple-form__item--input">
      </li>
      <li class="simple-form__item">
        <label for="fullName" class="simple-form__item--label">Полное имя:</label>
        <input type="text" value="${usr.fullName!''}" name="fullName" id="fullName" class="simple-form__item--input">
      </li>
      <li class="simple-form__item">
        <label for="email" class="simple-form__item--label">E-mail:</label>
        <input type="email" value="${usr.email!''}" name="email" id="email" class="simple-form__item--input">
      </li>
      <li class="simple-form__item">
        <label for="education" class="simple-form__item--label">Образование:</label>
        <input type="text" value="${usr.education!''}" name="education" id="education" class="simple-form__item--input">
      </li>
      <li class="simple-form__item">
        <label for="birth" class="simple-form__item--label">Дата рождения:</label>
        <input type="date" value="${usr.birth!''}" name="birth" id="birth" class="simple-form__item--input">
      </li>
      <li class="simple-form__item">
        <b class="simple-form__item--label">Роли:</b>
        <#list roles as role>
        <div class="simple-form__item">
          <label class="simple-form__item--label">
            <input type="checkbox" name="${role}" ${usr.roles?seq_contains(role)?string("checked", "")} class="simple-form__item--input">
            ${role}
            </label>
        </div>
        </#list>
      </li>
      <li class="simple-form__item">
        <b class="simple-form__item--label">Специальность:</b>
        <select name="fkSpeciality" class="simple-form__item--input">
        <#list specialitiesList as sl>
          <option value="${sl.id}" <#if usr.fkSpeciality.id == sl.id>selected</#if>>${sl.title}</option>
        </#list>
        </select>
      </li>
      <li class="simple-form__item">
        <b class="simple-form__item--label">Учебная группа:</b>
        <select name="fkTeachingGroup" class="simple-form__item--input">
        <#list teachingGroupsList as tgl>
          <option value="${tgl.id}" <#if usr.fkTeachingGroup.id == tgl.id>selected</#if>>${tgl.title}</option>
        </#list>
        </select>
      </li>
      <li class="simple-form__item">
        <button type="submit" class="simple-form__item--input">Сохранить изменения!</button>
      </li>
      <li class="simple-form__item">
        <a href="/logout">Выйти из-под сессии</a>
      </li>
    </ul>
    
  </form>

</@page.page>